package eu.epitech.tristan.todolist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Todolist extends AppCompatActivity {
    private ArrayList<Obj> items;
    private ArrayAdapter<Obj> itemsAdapter;
    private ListView lvItems;
    private AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent i = new Intent(getApplicationContext(), ViewItem.class);
            Obj a = (Obj) parent.getItemAtPosition(position);
            a.setPosition(position);
            i.putExtra("Obj", (Serializable)a);
            startActivityForResult(i, 100);
        }
    };
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 100){
            Obj tmp = (Obj)data.getSerializableExtra("Obj");
            if (items == null) {
                items = new ArrayList<Obj>();
            }
            if (tmp.getPosition() == -1)
                items.add(tmp);
            else
                items.set(tmp.getPosition(),tmp);
            itemsAdapter.notifyDataSetChanged();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = prefs.edit();
            try {
                JSONArray stock = new JSONArray();
                for (int i = 0; i < items.size(); i++) {
                    JSONObject json = new JSONObject();
                    json.put("content",items.get(i).getContent());
                    json.put("title",items.get(i).getTitle());
                    json.put("year",items.get(i).getYear());
                    json.put("month",items.get(i).getMonth());
                    json.put("day",items.get(i).getDay());
                    json.put("date",items.get(i).getDate());
                    json.put("hour",items.get(i).getHour());
                    json.put("min",items.get(i).getMinutes());
                    json.put("position",items.get(i).getPosition());
                    json.put("done",items.get(i).isDone());
                    stock.put(json);
                }
                editor.putString("listTask", stock.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            editor.commit();
            }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todolist);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
       // editor.clear();
        //editor.commit();
        lvItems = (ListView) findViewById(R.id.lvItems);
        items = new ArrayList<Obj>();
            String jsonString = prefs.getString("listTask", null);
            if (jsonString != null) {
                JSONArray json = null;
                try {
                    json = new JSONArray(jsonString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    for (int i = 0; i < json.length(); i++) {
                        Obj tm = new Obj();
                        tm.setContent(json.getJSONObject(i).getString("content"));
                        tm.setTitle(json.getJSONObject(i).getString("title"));
                        tm.setYear(json.getJSONObject(i).getInt("year"));
                        tm.setMonth(json.getJSONObject(i).getInt("month"));
                        tm.setDay(json.getJSONObject(i).getInt("day"));
                        tm.setHour(json.getJSONObject(i).getString("hour"));
                        tm.setMinutes(json.getJSONObject(i).getString("min"));
                        tm.setPosition(json.getJSONObject(i).getInt("position"));
                        tm.setDone(json.getJSONObject(i).getBoolean("done"));
                        items.add(tm);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        itemsAdapter = new ArrayAdapter<Obj>(this,
                android.R.layout.simple_list_item_1, items);
        lvItems.setAdapter(itemsAdapter);
        setupListViewListener();
        lvItems.setOnItemClickListener(listener);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_task: {
                Obj a = new Obj("");
                Intent i = new Intent(getApplicationContext(), ViewItem.class);
                i.putExtra("Obj", (Serializable)a);
                startActivityForResult(i, 100);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void setupListViewListener() {
        lvItems.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapter,
                                                   View item, int pos, long id) {
                        items.remove(pos);
                        itemsAdapter.notifyDataSetChanged();
                        return true;
                    }

                });
    }
}
