package eu.epitech.tristan.todolist;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.R.attr.y;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class ViewItem extends AppCompatActivity {
private EditText Title;
    private CheckBox checkBox;
    private EditText time;
    private EditText cal;
    private EditText Item;
private Button Back;
private int pos;
private Obj tmp;
    private String text;
    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            tmp.setYear(year);
            tmp.setMonth(monthOfYear);
            tmp.setDay(dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "MM/dd/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        cal.setText(sdf.format(myCalendar.getTime()));
        try {
            tmp.setDate(sdf.parse(String.valueOf(cal.getText())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);
        Title = (EditText) findViewById(R.id.Title);
        Item = (EditText) findViewById(R.id.Item);
        cal = (EditText) findViewById(R.id.Date);
        time = (EditText) findViewById(R.id.Time);
        Back = (Button) findViewById(R.id.Back);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tmp.setDone(!tmp.isDone());
            }
        });
        Back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent i  = getIntent();
        tmp = (Obj) i.getSerializableExtra("Obj");
        Title.setText(tmp.getTitle());
        Item.setText(tmp.getContent());
        if (tmp.getYear() != 0)
        {
            myCalendar.set(Calendar.YEAR, tmp.getYear());
            myCalendar.set(Calendar.MONTH, tmp.getMonth());
            myCalendar.set(Calendar.DAY_OF_MONTH, tmp.getDay());
            String myFormat = "MM/dd/yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            cal.setText(sdf.format(myCalendar.getTime()));
        }
        if (tmp.getHour() != "")
            time.setText( tmp.getHour() + ":" + tmp.getMinutes());
        checkBox.setChecked(tmp.isDone());
        Item.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tmp.setContent(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        Title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tmp.setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        cal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(ViewItem.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ViewItem.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String hour = "";
                        if (selectedHour < 10)
                            hour += "0";
                        hour += selectedHour;
                        String minutes = "";
                        if (selectedMinute < 10)
                            minutes += "0";
                        minutes += selectedMinute;
                        Calendar calen = Calendar.getInstance();
                        if (tmp.getDate() != null)
                            calen.setTime(tmp.getDate());
                        calen.set(Calendar.HOUR, selectedHour);
                        calen.set(Calendar.MINUTE, selectedMinute);
                        tmp.setDate(calen.getTime());
                        tmp.setHour(hour);
                        tmp.setMinutes(minutes);
                        time.setText( hour + ":" + minutes);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        Intent a = new Intent();
        a.putExtra("Obj", tmp);
        setResult(100, a);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
