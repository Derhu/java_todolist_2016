package eu.epitech.tristan.todolist;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Tristan on 19/01/2017.
 */
public class Obj implements Serializable{
    private String title = "";
    private String content = "";
    private int position = -1;
    private Date date = null;
    private int year = 0;
    private int month = 0;
    private int day = 0;
    private boolean done = false;
    private String hour = "";
    private String minutes = "";

    public Obj(String ti)
    {
        if (!ti.isEmpty())
        title = ti;
        else
        title = "";

    }

    public Obj() {
    done = false;
    }
    public String isFinished() {
        if (done == true)
            return ("\nDone");
        else
            return ("\nNot finished yet");
    }
    public String toString(){
        return (title + isFinished());
    }
    private String sub(String s) {
        if (s.length() < 10)
        return (s);
        else
            return (s.substring(0, 10));
    }
    public Date getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public boolean isDone() {
        return done;
    }
    public void setDone(boolean done) {
        this.done = done;
    }
}
